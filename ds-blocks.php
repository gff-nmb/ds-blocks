<?php

/**
 * Plugin Name:       DS Blocks
 * Description:       The wonderful content blocks for Digital Signage Screens.
 * Requires at least: 5.8
 * Requires PHP:      7.4
 * Version:           1.4
 * Author:            Adrian Suter
 * Author URI:        https://www.gff.ch/
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       ds-blocks
 */

declare( strict_types=1 );

use YahnisElsts\PluginUpdateChecker\v5\PucFactory;

/**
 * Absolute filesystem path to the `ds-blocks` plugin root file.
 */
define( 'DS_BLOCKS_ABS_PLUGIN_FILE', __FILE__ );

define( 'DS_BLOCKS_DOMAIN', 'ds-blocks' );

add_action( 'plugins_loaded', function () {
	load_plugin_textdomain( 'ds-blocks', false, 'ds-blocks/locales' );
} );

add_action( 'init', function (): void {
	register_block_type( __DIR__ . '/map-tile-toggles' );
	register_block_type( __DIR__ . '/map-panel' );
	register_block_type( __DIR__ . '/map' );

	$map_view_script_handle = generate_block_asset_handle( 'ds-blocks/ds-map', 'viewScript' );

	wp_set_script_translations(
		generate_block_asset_handle( 'ds-blocks/ds-map', 'editorScript' ),
		DS_BLOCKS_DOMAIN,
		plugin_dir_path( __FILE__ ) . 'locales/'
	);

	wp_set_script_translations(
		generate_block_asset_handle( 'ds-blocks/ds-map-panel', 'editorScript' ),
		DS_BLOCKS_DOMAIN,
		plugin_dir_path( __FILE__ ) . 'locales/'
	);

	wp_add_inline_script(
		$map_view_script_handle,
		'const DS_BLOCKS = ' . json_encode( [
			'map' => [
				'image_path'       => plugins_url( 'map/assets/', DS_BLOCKS_ABS_PLUGIN_FILE ),
				'marker_pin_svg'   => file_get_contents( plugin_dir_path( DS_BLOCKS_ABS_PLUGIN_FILE ) . 'map/assets/marker-pin.svg' ),
				'marker_label_svg' => file_get_contents( plugin_dir_path( DS_BLOCKS_ABS_PLUGIN_FILE ) . 'map/assets/marker-label.svg' ),
			],
		] ),
		'before'
	);
} );

///////////////////////////// PLUGIN UPDATE CHECKER ////////////////////////////

add_action( 'plugins_loaded', function () {
	$group_name  = 'gff-nmb';
	$plugin_name = basename( DS_BLOCKS_ABS_PLUGIN_FILE, '.php' );
	if ( class_exists( PucFactory::class ) ) {
		$update_checker = PucFactory::buildUpdateChecker(
			'https://gitlab.com/' . $group_name . '/' . $plugin_name . '/',
			DS_BLOCKS_ABS_PLUGIN_FILE
		);

		$vcs_api = $update_checker->getVcsApi();
		if ( method_exists( $vcs_api, 'enableReleasePackages' ) ) {
			$vcs_api->enableReleasePackages();
		} else {
			trigger_error(
				'PUC Updater can not update as the VCS API does not support release packages.',
				E_USER_ERROR
			);
		}
	}
} );
