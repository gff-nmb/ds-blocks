import { InnerBlocks, useBlockProps } from '@wordpress/block-editor';

export default function Save ({ attributes }) {
  const blockProps = useBlockProps.save();
  const { latitude, longitude, iconUrl, label } = attributes;

  const lat = parseFloat(latitude);
  const lon = parseFloat(longitude);

// noinspection JSXNamespaceValidation
  return (
    <div {...blockProps}>
      <div data-latitude={isNaN(lat) ? 0 : lat}
           data-longitude={isNaN(lon) ? 0 : lon}
           data-icon={iconUrl}
           data-label={label}>
        <InnerBlocks.Content/>
      </div>
    </div>
  );
}
