import { InnerBlocks, InspectorControls, MediaUpload, MediaUploadCheck, useBlockProps } from '@wordpress/block-editor';
import { Button, PanelBody, TextControl } from '@wordpress/components';
import { __ } from '@wordpress/i18n';
import './editor.scss';

const { Fragment } = wp.element;

export default function Edit ({ attributes, setAttributes }) {
  const TEMPLATE = [
    ['core/paragraph', { content: __('Content...', 'ds-blocks') }],
  ];

  const { latitude, longitude, iconId, iconUrl, label } = attributes;

  const onChangeLatitude = (newContent) => {
    setAttributes({ latitude: newContent });
  };

  const onChangeLongitude = (newContent) => {
    setAttributes({ longitude: newContent });
  };

  const onSelectMedia = (media) => {
    setAttributes({
      iconId: media.id,
      iconUrl: media.url
    });
  };

  const removeMedia = () => {
    setAttributes({
      iconId: 0,
      iconUrl: ''
    });
  };

  const onChangeLabel = (value) => {
    setAttributes({
      label: value
    });
  };

  let lat = parseFloat(latitude);
  if (isNaN(lat)) { lat = '-';}

  let lon = parseFloat(longitude);
  if (isNaN(lon)) { lon = '-'; }

  // noinspection JSXNamespaceValidation
  return (
    <Fragment>
      <InspectorControls>
        <PanelBody title={__('Geo Position', 'ds-blocks')} initialOpen={true}>
          <TextControl
            label={__('Latitude', 'ds-blocks')}
            value={latitude}
            onChange={onChangeLatitude}
          />
          <TextControl
            label={__('Longitude', 'ds-blocks')}
            value={longitude}
            onChange={onChangeLongitude}
          />
        </PanelBody>
        <PanelBody title={__('Icon & Label', 'ds-blocks')} initialOpen={true}>
          <div className={'ds-map-panel-panel-icon'}>
            <MediaUploadCheck>
              <MediaUpload
                onSelect={onSelectMedia}
                value={iconId}
                allowedTypes={['image']}
                render={({ open }) => (
                  <Button onClick={open} isDefault isLarge>
                    {__('Choose an icon', 'ds-blocks')}
                  </Button>
                )}
              />
            </MediaUploadCheck>
            {iconId !== 0 &&
              <MediaUploadCheck>
                <Button onClick={removeMedia} isLink isDestructive>{__('Remove icon', 'ds-blocks')}</Button>
              </MediaUploadCheck>
            }
          </div>

          <TextControl
            label={__('Label', 'ds-blocks')}
            value={label}
            onChange={onChangeLabel}
          />

        </PanelBody>
      </InspectorControls>
      <div {...useBlockProps()}>
        <div className="ds-map-panel-info">
          <div className="ds-map-panel-info-geo-position">
            {lat} / {lon}
          </div>
          {label !== '' &&
            <div className={'ds-map-panel-info-label-preview'}>
              {label}
            </div>
          }
          {iconId !== 0 &&
            <div className="ds-map-panel-info-icon-preview">
              <img src={iconUrl} alt=""/>
            </div>
          }
        </div>
        <InnerBlocks template={TEMPLATE}/>
      </div>
    </Fragment>
  );
}
