# DS Blocks

The WordPress Plugin **DS Blocks**.

## Dependencies

* [DS Core](https://gitlab.com/gff-nmb/ds-core)

## Authors and acknowledgment

Adrian Suter, [adrian.suter@gff.ch](mailto:adrian.suter@gff.ch)

## Copyright

[GFF Integrative Kommunikation GmbH](https://www.gff.ch/)

## License

[GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

## Development

### Staging

Always use a staging environment to develop and test things out before releasing.

### Build a new release

1. Set the new version `X.Y` in the file comments section of `ds-blocks.php`.
   Commit and push.
2. Add a new repository tag `X.Y` without writing release notes (a message is
   allowed).
4. Gitlab CI would automatically create a release asset zip file.
5. Once finished (see pipeline or release section), update the stable version in
   `readme.txt` to `X.Y` and commit and push.

### The map tiles

We use QGIS to generate the map tiles. They would be bound to the following coordinates:

- Southwest: 46.8993003°, 7.016752°
- Northeast: 47.2107725°, 7.4287738°

Note: If you are changing the bounds you need to change them in `map/src/view.js` also.

Note: [Transform coordinates](https://epsg.io/transform#s_srs=2056&t_srs=4326&ops=1676&x=2599253.4875000&y=1228870.7084000)

| Name         | Value                                                             |
|--------------|-------------------------------------------------------------------|
| Extent       | `2567853.0430,2599253.4875,1194330.2195,1228870.7084 [EPSG:2056]` |
| Minimum Zoom | `13`                                                              |
| Maximum Zoom | `18`                                                              |
| DPI          | `96`                                                              |
| Tile format  | `PNG`                                                             |
