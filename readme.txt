=== DS Blocks ===
Contributors: masterida
Donate link: https://www.gff.ch/
Requires at least: 5.0
Tested up to: 6.1
Requires PHP: 7.4
Stable tag: 1.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

The DS Blocks plugin.

== Description ==

The **DS Blocks** plugin.

== Changelog ==

= 1.4 =
* Remove PUCv4 updater compatibility.

= 1.3 =
* Add PUCv5 updater compatibility.

= 1.1 =
* Add new features.

= 1.0 =
* Initial version.
