const path = require('path');

module.exports = function (grunt) {
  const package_name = 'ds-blocks';

  // noinspection JSUnresolvedFunction
  grunt.initConfig({
    compress: {
      main: {
        options: {
          archive: function () {
            return '../' + package_name + '.zip';
          },
          mode: 'zip',
        },
        files: [
          {
            expand: true,
            cwd: '../',
            src: [
              '**',
              '!_dev/**',
              '!.gitignore',
              '!.gitlab-ci.yml',
              '!' + package_name + '.zip',
              '!map/src/**',
              '!map-panel/src/**',
              '!README.md',
            ],
            dest: package_name + '/',
          },
        ],
      },
    },
  });

  grunt.loadNpmTasks('grunt-contrib-compress');

  // Register the tasks.
  grunt.registerTask('build', ['compress']);

  // Note that the task would use a regular expression to find `__('...', 'ds-blocks')`
  // function calls. The regexp is not very sophisticated.
  grunt.registerTask('i18n-json', '', function () {
    const handles = {
      'ds-blocks-ds-map-editor-script': ['map/src/index.js', 'map/src/edit.js', 'map/src/save.js'],
      // 'ds-blocks-ds-map-view-script': ['map/src/view.js'],
      'ds-blocks-ds-map-panel-editor-script': ['map-panel/src/index.js', 'map-panel/src/edit.js', 'map-panel/src/save.js'],
    };

    const domain = 'ds-blocks';
    const regexp = new RegExp(/__\(\s*'(.*)',\s*'ds-blocks'\s*\)/, 'g');
    const locales = ['de_DE', 'fr_FR'];

    let match;
    for (const handle in handles) {
      const sources = handles[handle];

      const sourceMessages = {};
      for (let j = 0; j < sources.length; j++) {
        const source = grunt.file.read('../' + sources[j]);
        while ((match = regexp.exec(source)) !== null) {
          sourceMessages[match[1].replaceAll('\\\'', '\'')] = [];
        }
      }

      let json;
      for (let i = 0; i < locales.length; i++) {
        const locale = locales[i];
        const filePath = '../locales/' + domain + '-' + locale + '-' + handle + '.json';

        if (!grunt.file.exists(filePath)) {
          json = {
            'domain': domain,
            'locale_data': {}
          };
          json['locale_data'][domain] = {
            '': {
              'domain': domain,
              'plural-forms': 'n != 1',
              'lang': locale.replace('_', '-')
            }
          };
        } else {
          json = grunt.file.readJSON(filePath);
        }

        for (let sourceMessage in sourceMessages) {
          if (json['locale_data'][domain][sourceMessage] === undefined) {
            json['locale_data'][domain][sourceMessage] = [''];
          }
        }

        for (let message in json['locale_data'][domain]) {
          if (message === '') {continue;}
          if (sourceMessages[message] === undefined) {
            delete json['locale_data'][domain][message];
          }
        }

        grunt.file.write(filePath, JSON.stringify(json, null, 2));
      }
    }
  });
};
