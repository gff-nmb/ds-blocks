import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';
import Edit from './edit';
import Save from './save';
import './style.scss';

/**
 * @see https://developer.wordpress.org/block-editor/reference-guides/block-api/block-registration/
 */
registerBlockType('ds-blocks/ds-map', {
  example: {
    innerBlocks: [
      {
        name: 'ds-blocks/ds-map-panel',
        attributes: {},
        innerBlocks: [
          {
            name: 'core/paragraph',
            attributes: {
              /* translators: example text. */
              content: __(
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent et eros eu felis.',
                'ds-blocks'
              ),
            },
          },
        ],
      },
    ],
  },
  edit: Edit,
  save: Save,
});
