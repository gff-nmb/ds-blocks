import { InnerBlocks, useBlockProps } from '@wordpress/block-editor';

export default function Save ({ attributes }) {
  const {
    panelsSize,
    initLatitude,
    initLongitude,
    initZoom,
    boundsNorth,
    boundsEast,
    boundsSouth,
    boundsWest,
    layers
  } = attributes;

  let dataLayers = '[]';
  try {
    dataLayers = JSON.stringify(JSON.parse(layers));
  } catch (e) {
  }

  const blockProps = useBlockProps.save();

  // noinspection JSXNamespaceValidation
  return (
    <div {...blockProps}>
      <div
        className="ds-map-container"
        data-init-latitude={initLatitude}
        data-init-longitude={initLongitude}
        data-init-zoom={initZoom}
        data-bounds-north={boundsNorth}
        data-bounds-east={boundsEast}
        data-bounds-south={boundsSouth}
        data-bounds-west={boundsWest}
        data-layers={dataLayers}
      ></div>
      <div className="ds-map-panels" data-size={panelsSize}>
        <InnerBlocks.Content/>
      </div>
    </div>
  );
}
