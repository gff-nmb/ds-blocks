/** @var {{map: {image_path: string, marker_pin_svg: string, marker_label_svg: string}}} DS_BLOCKS */

/**
 * Important note: Depends on the ds-theme.
 */

import domReady from '@wordpress/dom-ready';
import * as L from '../../_dev/node_modules/leaflet/dist/leaflet-src.esm';
import '../../_dev/node_modules/leaflet/dist/leaflet.css';

(function ($, win) {
  // A list of all map instances.
  const maps = [];

  // A list of all layers (grouped by maps).
  const layers = [];

  const __post_container_selector = '.post-container';
  const block_map_selector = '.wp-block-ds-blocks-ds-map';
  const block_map_panel_selector = '.wp-block-ds-blocks-ds-map-panel';
  const marker_active_class_name = 'marker-active';
  const map_panel_hidden_class_name = 'is-hidden';
  const map_container_selector = '.ds-map-container';

  const map_panel_navigation_class = 'ds-map-panel-navigation';

  domReady(function () {
    // Set the general image path for leaflet icons.
    L.Icon.Default.prototype.options.imagePath = DS_BLOCKS['map']['image_path'];

    /**
     * The object for a map panel marker. Extends the L.Marker object.
     */
    const Map_Panel_Marker = L.Marker.extend({
      options: {
        mapPanel: null,
      },
      getMapPanel: function () {
        return this.options.mapPanel;
      },
      activate: function () {
        $(this._icon).addClass(marker_active_class_name);
      },
      deactivate: function () {
        $(this._icon).removeClass(marker_active_class_name);
      }
    });

    // The body.
    const $body = $('body');

    /**
     * Shows the given map panel. The method would also update the markers as
     * well as the navigation icon buttons.
     *
     * @param {jQuery} $map_panel
     * @param {boolean} center_marker
     */
    const show_map_panel = ($map_panel, center_marker) => {
      const $map_block = $map_panel.closest(block_map_selector);
      hide_visible_map_panels($map_block);

      $map_panel.removeClass(map_panel_hidden_class_name);

      const marker = $map_panel.data('marker');
      if (marker instanceof Map_Panel_Marker) {
        marker.activate();
        if (center_marker) {
          const map_index = $map_block.find(map_container_selector).data('map');
          if (map_index !== undefined && maps[map_index] !== undefined) {
            const map = maps[map_index];
            map.setView(marker.getLatLng(), map.getZoom(), { animate: true });
          }
        }
      }

      const $map_panel_navigation = $map_block.find('.' + map_panel_navigation_class);
      $map_panel_navigation.toggleClass('has-prev', $map_panel.prev(block_map_panel_selector).length > 0);
      $map_panel_navigation.toggleClass('has-next', $map_panel.next(block_map_panel_selector).length > 0);
    };

    /**
     * Hides the given map panel. The method would also update the associated
     * marker.
     *
     * @param {jQuery} $map_panel
     */
    const hide_map_panel = ($map_panel) => {
      win.DS_Theme.os_scroll($map_panel, 0);

      $map_panel.addClass(map_panel_hidden_class_name);

      const marker = $map_panel.data('marker');
      if (marker instanceof Map_Panel_Marker) {
        marker.deactivate();
      }
    };

    /**
     * Gets all visible map panels for a given map block.
     *
     * @param {jQuery} $map_block
     * @returns {*}
     */
    const get_visible_map_panels = ($map_block) => {
      return $map_block.find(block_map_panel_selector + ':not(.' + map_panel_hidden_class_name + ')');
    };

    /**
     * Hide all visible map panels for the given map block. The method would
     * also update all associated markers.
     *
     * @param {jQuery} $map_block
     */
    const hide_visible_map_panels = ($map_block) => {
      get_visible_map_panels($map_block).each(function () {
        hide_map_panel($(this));
      });
    };

    /**
     * Jumps either to the next or the previous map panel.
     *
     * @param {jQuery} $map_block
     * @param {int} direction Positive integer for `next`, else `prev`.
     */
    const jump_map_panel = ($map_block, direction) => {
      const $current_visible_map_panel = get_visible_map_panels($map_block).first();
      show_map_panel(
        (direction > 0) ?
          $current_visible_map_panel.next(block_map_panel_selector) :
          $current_visible_map_panel.prev(block_map_panel_selector),
        true
      );
    };

    /**
     * Event handler for the click event on map markers.
     *
     * @param {PointerEvent} e
     */
    const marker_click = function (e) {
      const marker = e.target;

      if (!(marker instanceof Map_Panel_Marker)) {
        console.error('Sorry, clicked marker is not an instance of `Map_Panel_Marker`.');
        return;
      }

      show_map_panel(marker.getMapPanel(), false);
    };

    /**
     * Gets the float value of a `data-` attribute or the given default value.
     *
     * @param {jQuery} $element
     * @param {string} data_name
     * @param {number} default_value
     * @returns {number}
     */
    const element_get_data_float = ($element, data_name, default_value) => {
      let value = parseFloat($element.data(data_name));
      if (isNaN(value)) {
        return default_value;
      }

      return value;
    };

    /**
     * Gets the int value of a `data-` attribute or the given default value.
     *
     * @param {jQuery} $element
     * @param {string} data_name
     * @param {number} default_value
     * @returns {number}
     */
    const element_get_data_int = ($element, data_name, default_value) => {
      let value = parseInt($element.data(data_name));
      if (isNaN(value)) {
        return default_value;
      }

      return value;
    };

    /* *************************************************************************

    Register the `ds-enter` event handler that would check if there are map
    blocks on the current post container. If so, the handler would reset the
    whole map (center, layer visibilities, introduction map panel).

    ************************************************************************* */
    $body.on('ds-enter', __post_container_selector, evt => {
      const $post_container = $(evt.currentTarget);

      const $map_blocks = $post_container.find(block_map_selector);
      $map_blocks.each(function () {
        // There are map blocks, so we need to reset the map.
        const $map_block = $(this);

        $map_block.find(map_container_selector).each(function () {
          const $map_container = $(this);
          const map_index = $map_container.data('map');

          /** @var {Map} map */
          const map = maps[map_index];

          const init_latitude = element_get_data_float($map_container, 'init-latitude', 47.1331);
          const init_longitude = element_get_data_float($map_container, 'init-longitude', 7.24305);
          const init_zoom = element_get_data_int($map_container, 'init-zoom', 15);

          map.setView([init_latitude, init_longitude], init_zoom, { animate: false });

          const layer_definitions = $map_container.data('layers');
          $.map(layer_definitions, (layer_definition, layer_key) => {
            if (layer_definition['type'] === 'toggle_hidden') {
              win.DS_Map.get_tile_layer(map_index, layer_key).setOpacity(0);
            } else if (layer_definition['type'] === 'toggle_visible') {
              win.DS_Map.get_tile_layer(map_index, layer_key).setOpacity(1);
            }
          });

          // Inform the map-tile-toggles that we might have changed the layer visibility.
          if (win.DS_Map_Tile_Toggles !== undefined) {
            win.DS_Map_Tile_Toggles.sync_toggles($map_block);
          }

          window.setTimeout(function () { map.invalidateSize(); }, 1);
        });

        // Show the first map panel (the introduction map panel) of this map.
        show_map_panel($map_block.find(block_map_panel_selector).first(), false);
      });
    });

    /* *************************************************************************

    Registers the `ds-leave` event handler that would reset overlay scrolls.

    ************************************************************************* */
    $body.on('ds-leave', __post_container_selector, evt => {
      const $post_container = $(evt.currentTarget);

      const $map_blocks = $post_container.find(block_map_selector);
      $map_blocks.each(function () {
        const $map_block = $(this);

        $map_block.find(block_map_panel_selector + ':visible').each(function () {
          const $map_panel = $(this);
          win.DS_Theme.os_scroll($map_panel, 0);
        });
      });
    });

    /* *************************************************************************

    Registers the `click` event handler on elements with a `data-map-toggle`
    attribute. This would be used to toggle the layers.

    ************************************************************************* */
    $body.on('click', '[data-map-toggle]', function () {
      const $map_toggle_element = $(this);
      const toggle_layer_key = $map_toggle_element.data('map-toggle');
      const map_layers = layers[$map_toggle_element.data('map-target')];

      /** @var {GridLayer} map_layer */
      const map_layer = map_layers[toggle_layer_key];
      if (map_layer.options.opacity === 0) {
        map_layer.setOpacity(1);
        $map_toggle_element.find('[type="checkbox"]').prop('checked', true);
      } else {
        map_layer.setOpacity(0);
        $map_toggle_element.find('[type="checkbox"]').prop('checked', false);
      }
    });

    /* *************************************************************************

    Initialize the map block as well as associated map panels and instantiate
    the corresponding leaflet maps.

    ************************************************************************* */
    $(block_map_selector).each(function () {
      const $map_block_element = $(this);

      // Get the index for the next map instance.
      const map_index = maps.length;

      // Set the size (width or height) for the panel.
      const $map_panels = $map_block_element.find('.ds-map-panels');
      if ($map_block_element.css('flex-direction').substring(0, 3) === 'row') {
        $map_panels.css('width', $map_panels.data('size'));
      } else {
        $map_panels.css('height', $map_panels.data('size'));
      }

      // Get the map container (the dom element the map would be loaded into).
      const $map_container = $map_block_element.find('.ds-map-container');

      // Set the map bounds.
      const bounds_attribute_prefix = 'bounds-';
      const bounds_north = element_get_data_float($map_container, bounds_attribute_prefix + 'north', 47.2183818);
      const bounds_east = element_get_data_float($map_container, bounds_attribute_prefix + 'east', 7.4357538);
      const bounds_south = element_get_data_float($map_container, bounds_attribute_prefix + 'south', 47.0056805);
      const bounds_west = element_get_data_float($map_container, bounds_attribute_prefix + 'west', 7.0350773);
      const bounds = L.latLngBounds(
        L.latLng(bounds_south, bounds_west),
        L.latLng(bounds_north, bounds_east)
      );

      // Instantiate the map.
      const map = L.map($map_container[0], {
        attributionControl: false,
        maxBounds: bounds,
        maxBoundsViscosity: 1
      });
      map.zoomControl.setPosition('bottomleft');

      // Put the map into the maps array as init the corresponding layers as well.
      maps[map_index] = map;
      layers[map_index] = {};

      // Put the `map_index` into the map container for later references.
      $map_container.data('map', map_index);

      // Add map layers.
      let layer_definitions = $map_container.data('layers');
      $.map(layer_definitions, (layer_definition, layer_key) => {
        const options = { tms: false };

        if (layer_definition['url'] === undefined) {
          return;
        }

        if (layer_definition['minZoom'] !== undefined) {
          options['minZoom'] = layer_definition['minZoom'];
        }

        if (layer_definition['maxZoom'] !== undefined) {
          options['maxZoom'] = layer_definition['maxZoom'];
        }

        // Note: The visibility of the layers would be set in the `ds-enter` event
        //       handler.

        // Note: Currently we support tile layers only. But it should be easy to
        //       make this dependent on a layer_definition property.
        layers[map_index][layer_key] = L.tileLayer(layer_definition['url'], options).addTo(map);
      });

      /* ***********************************************************************

      Initialize the map panels of the map. The first map panel is actually the
      introduction panel. It would never have a marker associated with.

      *********************************************************************** */
      let map_panel_counter = 0;

      // Put all map panels into a wrapper (required for scrolling).
      const $t = $('<div class="ds-map-panels-wrapper"></div>');
      $map_panels.append($t);
      $map_panels.find(block_map_panel_selector).appendTo($t);

      $map_panels.find(block_map_panel_selector).each(function () {
        const $map_panel = $(this);

        $map_panel.addClass('is-hidden');

        if (map_panel_counter === 0) {
          // This is the introduction map-panel.
        } else {
          // This is a marker map-panel.
          const icon = $map_panel.find('>[data-icon]').data('icon');
          const label = $map_panel.find('>[data-label]').data('label');

          let marker_icon = L.divIcon({
            html: DS_BLOCKS['map']['marker_pin_svg'] + '<div class="ds-map-marker-number">' + map_panel_counter + '</div>',
            className: 'ds-map-marker-pin-numeric',
            iconSize: [50, 67.2371770392],
            iconAnchor: [50 / 2, 67.2371770392],
          });

          if (label !== '') {
            marker_icon = L.divIcon({
              html: DS_BLOCKS['map']['marker_label_svg'] + '<div class="ds-map-marker-text"><div>' + label + '</div></div>',
              className: 'ds-map-marker-label-text',
              iconSize: [40, 17],
              iconAnchor: [20, 17],
            });
          } else if (icon !== '') {
            marker_icon = L.divIcon({
              html: DS_BLOCKS['map']['marker_pin_svg'] + '<div class="ds-map-marker-icon"><img class="ds-map-marker-icon-img" src="' + icon + '" alt=""></div>',
              className: 'ds-map-marker-pin-icon',
              iconSize: [50, 67.2371770392],
              iconAnchor: [50 / 2, 67.2371770392],
            });
          } else {
            $map_panel.find('h2').first().prepend(map_panel_counter + ': ');
          }

          // .ds-map-marker-pin-numeric
          // => svg.ds-map-marker-pin
          //    div.ds-map-marker-number

          // .ds-map-marker-pin-icon
          // => svg.ds-map-marker-pin
          //    div.ds-map-marker-icon
          //    => img.ds-map-marker-icon-img

          // .ds-map-marker-label-text
          // => svg.ds-map-marker-label
          // => svg.ds-map-marker-arrow-head

          const marker = new Map_Panel_Marker([
            $map_panel.find('>[data-latitude]').data('latitude'),
            $map_panel.find('>[data-longitude]').data('longitude')
          ], { icon: marker_icon, mapPanel: $map_panel })
            .addTo(map)
            .on('click', marker_click);

          $map_panel.data('marker', marker);
        }

        $map_panel.css('height', '100%');
        win.DS_Theme.os_init($map_panel);

        map_panel_counter++;
      });

      if (map_panel_counter > 1) {
        $map_panels.addClass('ds-map-panels-with-navigation');

        /* *********************************************************************
        Add the navigation bar to navigate the map-panels.
        ********************************************************************* */
        const $map_panel_navigation = $('<div class="' + map_panel_navigation_class + '"></div>');
        const $prev_icon_button = $('#icon-button-prev').children().clone().addClass('prev-required');
        const $next_icon_button = $('#icon-button-next').children().clone().addClass('next-required');
        $prev_icon_button.on('click', (event) => {
          event.preventDefault();

          jump_map_panel($map_block_element, -1);
        });
        $next_icon_button.on('click', (event) => {
          event.preventDefault();

          jump_map_panel($map_block_element, 1);
        });

        $map_panel_navigation.append($prev_icon_button);
        $map_panel_navigation.append($next_icon_button);
        $map_panels.prepend($map_panel_navigation);
      }

      show_map_panel($map_panels.find(block_map_panel_selector).first(), false);

      // window.setInterval(function () {
      //   console.debug('Zoom Level: ' + map.getZoom());
      // }, 5000);
    });

    if (win.DS_Map_Tile_Toggles) {
      win.DS_Map_Tile_Toggles.init();
    }
  });

  win.DS_Map = {
    /**
     * Gets the closest `map` block for the given element.
     *
     * @param {jQuery} $element
     * @returns {jQuery}
     */
    get_closest_block_map: ($element) => {
      return $element.closest(block_map_selector);
    },
    /**
     * Gets the map container given a `map` block.
     *
     * @param {jQuery} $block_map
     * @returns {jQuery}
     */
    get_map_container: ($block_map) => {
      return $block_map.find(map_container_selector);
    },
    /**
     * Gets the tile layer.
     *
     * @param {number} map_index
     * @param {string} layer_key
     * @returns {GridLayer|undefined}
     */
    get_tile_layer: (map_index, layer_key) => {
      return layers[map_index][layer_key];
    },
    /**
     * Gets all maps.
     *
     * @returns {*[]}
     */
    get_maps: () => {
      return maps;
    }
  };
})(jQuery, window);
