import { InnerBlocks, InspectorControls, useBlockProps } from '@wordpress/block-editor';
import {
  __experimentalHStack as HStack, __experimentalNumberControl as NumberControl,
  __experimentalUnitControl as UnitControl, PanelBody, TextareaControl, TextControl
} from '@wordpress/components';
import { __ } from '@wordpress/i18n';
import './editor.scss';

const { Fragment } = wp.element;

export default function Edit ({ attributes, setAttributes }) {
  const ALLOWED_BLOCKS = ['ds-blocks/ds-map-panel'];
  const TEMPLATE = [
    [
      'ds-blocks/ds-map-panel',
      {},
      [['core/paragraph', { content: __('Content...', 'ds-blocks') }]],
    ],
    [
      'ds-blocks/ds-map-panel',
      {},
      [['core/paragraph', { content: __('Content...', 'ds-blocks') }]],
    ],
  ];

  const {
    panelsSize,
    initLatitude,
    initLongitude,
    initZoom,
    boundsNorth,
    boundsEast,
    boundsSouth,
    boundsWest,
    layers
  } = attributes;

  const units = [
    { value: 'px', label: 'px', default: 300 },
    { value: '%', label: '%', default: 10 },
  ];

  const onChangeAttribute = (propName) => {
    return (newContent) => {
      const attributes = {};
      attributes[propName] = newContent;

      setAttributes(attributes);
    };
  };

  let layersJsonValid = false;
  try {
    JSON.parse(layers);
    layersJsonValid = true;
  } catch (e) { }

  // noinspection JSXNamespaceValidation
  return (
    <Fragment>
      <InspectorControls>
        <PanelBody title={__('Map Settings', 'ds-blocks')}>
          <TextControl
            label={__('Initial latitude', 'ds-blocks')}
            value={initLatitude}
            onChange={onChangeAttribute('initLatitude')}
          />
          <TextControl
            label={__('Initial longitude', 'ds-blocks')}
            value={initLongitude}
            onChange={onChangeAttribute('initLongitude')}
          />
          <NumberControl
            className={'components-base-control'}
            label={__('Initial zoom level', 'ds-blocks')}
            value={initZoom}
            min={14}
            max={17}
            onChange={onChangeAttribute('initZoom')}
          />
          <HStack alignment="top">
            <TextControl
              label={__('Bounds South', 'ds-blocks')}
              value={boundsSouth}
              onChange={onChangeAttribute('boundsSouth')}
            />
            <TextControl
              label={__('Bounds West', 'ds-blocks')}
              value={boundsWest}
              onChange={onChangeAttribute('boundsWest')}
            />
          </HStack>
          <HStack alignment="top">
            <TextControl
              label={__('Bounds North', 'ds-blocks')}
              value={boundsNorth}
              onChange={onChangeAttribute('boundsNorth')}
            />
            <TextControl
              label={__('Bounds East', 'ds-blocks')}
              value={boundsEast}
              onChange={onChangeAttribute('boundsEast')}
            />
          </HStack>
          <TextareaControl
            className={layersJsonValid ? '' : 'ds-map-invalid'}
            label={__('Map Layers', 'ds-blocks')}
            value={layers}
            rows="10"
            onChange={onChangeAttribute('layers')}
          />
          <UnitControl
            label={__('Panels Size', 'ds-blocks')}
            value={panelsSize}
            units={units}
            onChange={onChangeAttribute('panelsSize')}
          />
        </PanelBody>
      </InspectorControls>
      <div {...useBlockProps()}>
        <InnerBlocks
          allowedBlocks={ALLOWED_BLOCKS}
          template={TEMPLATE}
        />
      </div>
    </Fragment>
  );
}
