import { registerBlockType } from '@wordpress/blocks';
import Edit from './edit';
import Save from './save';
import './style.scss';

registerBlockType('ds-blocks/ds-map-tile-toggles', {
  edit: Edit,
  save: Save,
});
