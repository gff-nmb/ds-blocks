(function (win, $) {
  win.DS_Map_Tile_Toggles = {
    /**
     * Synchronizes the map tile toggle buttons with the actual tile visibilities.
     *
     * @param {jQuery|HTMLElement} $block_map
     */
    sync_toggles: ($block_map) => {
      const $map_container = win.DS_Map.get_map_container($block_map);

      $block_map.find('[data-map-target=' + $map_container.data('map') + '][data-map-toggle]').each(
        function () {
          const $t = $(this);

          const map_layer = win.DS_Map.get_tile_layer(
            $t.data('map-target'),
            $t.data('map-toggle')
          );

          if (map_layer === undefined) {
            return;
          }

          $t.find('.toggle').prop('checked', (map_layer.options.opacity !== 0));
        }
      );
    },
    /**
     * This method would be called from the domReady callback of the `ds-map/view.js`.
     */
    init: () => {
      let counter = 0;

      // Get the body.
      const $body = $('body');

      /* *************************************************************************

      Setup.

      ************************************************************************* */
      $('.wp-block-ds-blocks-ds-map-tile-toggles').each(function () {
        const $block = $(this);

        const $block_map = win.DS_Map.get_closest_block_map($block);
        if ($block_map.length === 0) {
          return;
        }

        const $map_container = win.DS_Map.get_map_container($block_map);
        const map_index = $map_container.data('map');

        const layer_definitions = $map_container.data('layers');
        $.map(layer_definitions, (layer_definition, layer_key) => {
          if (layer_definition['type'] === undefined) {
            return;
          }

          const uid = 'ds-map-tile-toggle-' + (counter++);
          const label = layer_definition['label'];

          $block.append('<div class="toggle-button" data-map-target="' + map_index + '" data-map-toggle="' + layer_key + '"><div>' + label + '</div><div>' +
            '<input type="checkbox" class="toggle" id="' + uid + '">' +
            '<label for="' + uid + '">' +
            '<span class="on"></span>' +
            '<span class="off"></span>' +
            '</label></div></div>');
        });

        // Make sure that a click on a toggle would be forwarded to the corresponding toggle-button.
        $body.on('click', '[data-map-toggle] .toggle', function (event) {
          event.preventDefault();

          $(this).closest('.toggle-button').click();
        });
      });
    }
  };
})(window, jQuery);
