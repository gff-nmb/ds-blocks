import { useBlockProps } from '@wordpress/block-editor';

export default function Save () {
  const blockProps = useBlockProps.save();

  // noinspection JSXNamespaceValidation
  return (
    <div {...blockProps}></div>
  );
}
