import { useBlockProps } from '@wordpress/block-editor';
import './editor.scss';

const { Fragment } = wp.element;

export default function Edit () {
  // noinspection JSXNamespaceValidation
  return (
    <Fragment>
      <div {...useBlockProps()}>
        Schalter für Tile-Layers
      </div>
    </Fragment>
  );
}
